// 1.	Maak een functie die random een element uit de volgende array retourneert.

const lapRounds = [2.99,  3.00, 3.01, 4.01, 2.79, 2.88, 3.10, 4.12];

const getRandomTime = function(){
    return lapRounds[Math.floor((Math.random() * lapRounds.length))];
}

console.log(getRandomTime());


/* 2.	Gegeven is de volgende 2-dimensionale array van al mijn platen:
const allMyRecords = [
    ["The Who - Pinball Wizard", "Rolling Stones - Exile on main street", "Police - Message in a bottle"],
    ["De Dijk - Alle 40 Goed", "Het Goede Doel - Belgie", "Doe Maar - skunk"]
];
Console.log alle waarden uit deze 2-dimensionale array. Tip: gebruik een dubbele loop.*/

const allMyRecords = [
    ["The Who - Pinball Wizard", "Rolling Stones - Exile on main street", "Police - Message in a bottle"],
    ["De Dijk - Alle 40 Goed", "Het Goede Doel - Belgie", "Doe Maar - skunk"]
];
 
for(let i = 0; i < allMyRecords.length; i++) {
    currentArray = allMyRecords[i];
    for(let i = 0; i < currentArray.length; i++) {
        console.log(currentArray[i]);
    }
 }


 // 4.
const lapRoundsPlus = [2.99,  3.00, 3.01, 4.01, 2.79, 2.88, 3.10, 4.12];

const timeCheck = function(lapRound) {
    return lapRound < 4;
}

const filteredLapRoundsPlus = function() {
    return lapRoundsPlus.filter(timeCheck);
}

console.log(filteredLapRoundsPlus());