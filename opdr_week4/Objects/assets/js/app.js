/* 1.	Schrijf de volgende array om in JSON. De array beschrijft rondetijden van een hardloper. Wat zijn de namen (keys) van de properties?
const lapRounds = [55.99,  63.00, 63.01, 54.01, 62.79, 52.88, 53.10, 54.12]; */

const lapRounds = [
    {
        lapNumber: 1,
        lapTime: 55.99
    },
    {
        lapNumber: 2,
        lapTime: 63.00
    },
    {
        lapNumber: 3,
        lapTime: 63.01
    },
    {
        lapNumber: 4,
        lapTime: 54.01
    },
    {
        lapNumber: 5,
        lapTime: 62.79
    },
    {
        lapNumber: 6,
        lapTime: 52.88
    },
    {
        lapNumber: 7,
        lapTime: 53.10
    },
    {
        lapNumber: 8,
        lapTime: 54.12
    },
]

console.log(lapRounds);


/* 2.	Gegeven is een array met drie objecten:
const teachers = [
    {
        name: “Loek”,
        profession: “Teacher”,
        brand: “Linux”
    },
    {
        name: “Daan”,
        profession: “Teacher”,
        brand: “Arduino”
    },
    {
        name: “Rimmert”,
        profession: “Teacher”,
        brand: “Apple”
    }
]
Gebruik een for-loop of foreach-loop en print voor elk object de regel: “I have a [profession] named [name] and he likes to work on a [brand] computer”.
De vierkante haken zijn placeholders voor de bijbehorende properties uit de objecten hierboven. */

const teachers = [
    {
        name: "Loek",
        profession: "Teacher",
        brand: "Linux"
    },
    {
        name: "Daan",
        profession: "Teacher",
        brand: "Arduino"
    },
    {
        name: "Rimmert",
        profession: "Teacher",
        brand: "Apple"
    }
]

const printLines = function() {
    for(let i = 0; i < teachers.length; i++) {
        const element = teachers[i];
        return(`I have a ${element.profession} named ${element.name} and he likes to work on a ${element.brand} computer.`);
        
    }
}

console.log(printLines());


/* 3.	[Pittig]. Gegeven is de array met objecten uit de vorige vraag. Voeg twee properties “hoursPerWeek” en “salary” toe.
Verzin zelf bijpassende waardes. Voeg nu een methode “salaryPerHour” toe, met daarin een functie die print hoeveel de docenten per uur verdienen.*/

const salaryPerHour = function() {
    return `${this.name} earns ${this.salaryPerHour} per hour`;
}

const teachersPlus = [
    {
        name: "Loek",
        profession: "Teacher",
        brand: "Linux",
        hoursPerWeek: 40,
        salary: 850,
        salaryPerHour: salaryPerHour()
    },
    {
        name: "Daan",
        profession: "Teacher",
        brand: "Arduino",
        hoursPerWeek: 34,
        salary: 700,
        salaryPerHour: salaryPerHour()
    },
    {
        name: "Rimmert",
        profession: "Teacher",
        brand: "Apple",
        hoursPerWeek: 22,
        salary: 200,
        salaryPerHour: salaryPerHour()
    }
]

console.log(teachersPlus[0].salaryPerHour);