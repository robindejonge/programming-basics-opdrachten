// 1.	Maak een while loop die kijkt of een in een reeks getallen deelbaar zijn door 4.

for (i = 1; i <= 50; i++) {
    if (i % 4 == 0) {
        console.log(`${i} is deelbaar door 4`);
    } else {
        console.log(`${i} is niet deelbaar door 4`);
    }
}


/*2.	Een Fibonacci getallenreeks is bijvoorbeeld: 
1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, …
Je ziet hier duidelijk dat elk getal de som is van de 2 voorafgaande getallen.
Schrijf een berekening van een Fibonacci reeks met while loop. Gebruik maximaal 10 getallen.
Console.log ook de gehele getallenreeks.*/

let fiboReeks = [1, 1];
let fiboMaximum = fiboReeks.length;
let newNumber = '';

while (fiboMaximum <= 10) {
    newNumber = fiboReeks[fiboMaximum - 1] + fiboReeks[fiboMaximum - 2]
    fiboReeks.push(newNumber);
    fiboMaximum++;
}

console.log(fiboReeks);


/* 3.	Gegeven is een array[2, 4, 8, 9, 12, 13] gebruik een for-loop om al deze getallen
bij elkaar op te tellen.*/

const sum = [2, 4, 8, 9, 12, 13];
let totalNumber = 0;

for(let index = 0; index < sum.length; index++) {
    totalNumber += sum[index];
}
console.log(totalNumber);