// 1.

let fiboReeks = [1, 1];

const fibonacciReeks = function(fiboReeks) {
    let fiboMaximum = fiboReeks.length;
    let newNumber = '';

    while (fiboMaximum <= 10) {
        newNumber = fiboReeks[fiboMaximum - 1] + fiboReeks[fiboMaximum - 2]
        fiboReeks.push(newNumber);
        fiboMaximum++;
    }
    return fiboReeks;
}

console.log(fibonacciReeks(fiboReeks));


// 2.

const currentYear = 2018;

const newYearCountdown = function(currentYear) {
    for (let i = 10; i >= 0; i--) {
        console.log(i);
        if(i == 0) {
            console.log(`Happy ${currentYear}!`);
        } else {
            
        }
    }
}

console.log(newYearCountdown(currentYear));


// 3.

const sayHello = function() {
    const message = 'Hello World!';
    return message;
}

console.log(sayHello());