//variables-data-types

// 1.
let number = 8;

if (number % 2 == 0) {
    console.log(number + ' is an even number');
} else {
    console.log(number + ' is an odd number');
}

// 2.
const str = 'Programming is not so cool';
console.log(str.replace('not ', ''));

// 3.
if (1400 == 'Ik woon in Naboo') {
    console.log('true');
} else {
    console.log('false');
}
// Dit is geen slimme vraag omdat je altijd false zal krijgen als uitkomst wanneer je een integer met een string probeert te vergelijken.