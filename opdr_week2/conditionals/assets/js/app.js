//Conditionals

// 1.
let grade = 4;

if (grade >= 0 && grade < 6) {
    console.log(grade);
    console.log('Allemaal onvoldoende!');
} else if (grade >= 6 && grade < 7) {
    console.log(grade);
    console.log('Jullie hebben allemaal een voldoende!');
} else if (grade >= 7 && grade < 9) {
    console.log(grade)
    console.log('Goed!');
} else if (grade >= 9 && grade <= 10) {
    console.log(grade);
    console.log('Uitmuntend!');
} else {
    console.log(grade + ' is geen geldig cijfer');
}


// 2.
switch (grade) {
    case grade >=0 && grade < 6:
        console.log(grade);
        console.log('Allemaal onvoldoende!');
        break;
    case grade >= 6 && grade < 7:
        console.log(grade);
        console.log('Jullie hebben allemaal een voldoende!');
        break;
    case grade >= 7 && grade < 9:
        console.log(grade);
        console.log('Goed!');
        break;
    case grade >= 9 && grade <= 10:
        console.log(grade);
        console.log('Uitmuntend!');
        break;
    default:
        console.log(grade);
        console.log(grade + ' is geen geldig cijfer!');
}


// 3.
let purchasedBook = true;
let job = 'teacher';
let inTrain = true;

if (purchasedBook == true && job == 'teacher' && inTrain == true) {
    console.log('finally I can enjoy my book!');
} else if (purchasedBook == false && job =='teacher' && inTrain == true) {
    console.log('I need to purchase a book to read.');
} else if (purchasedBook == true && job == 'teacher' && inTrain == false) {
    console.log('I can read this book in the train.');
} else if (purchasedBook == true && job != 'teacher' && inTrain == true) {
    console.log('Why should I read this book?');
} else if (purchasedBook == false && job !='teacher' && inTrain == true) {
    console.log('The train is nice!');
} else if (purchasedBook == true && job != 'teacher' && inTrain == false) {
    console.log('Why do I have this book?');
} else {
    console.log('I am going home.');
}