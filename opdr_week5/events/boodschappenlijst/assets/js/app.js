window.addEventListener('load', init)

//all variables are declared here
let productList;
let totalPrice;
let submitButton;
let removeButtonText = 'remove'

//gather all required HTML elements
function init() {
    productList = document.getElementById('product-list');
    totalPrice = document.getElementById('total-price');
    submitButton = document.getElementById('submit');
    submitButton.addEventListener('click', addProductToList);
    productList.addEventListener('click', removeProductFromList);
}

//add a product to productList
function addProductToList(event) {
    event.preventDefault();

    const product = document.getElementById('product').value;
    const price = document.getElementById('price').value;
    console.log(product);
    console.log(price);

    //creates new cells in table and inserts product and price
    const newTr = document.createElement('tr');

    const tableProduct = document.createElement('td');
    const tableProductValue = document.createTextNode(product);
    tableProduct.appendChild(tableProductValue);

    const tablePrice = document.createElement('td');
    const tablePriceValue = document.createTextNode(price);
    tablePrice.appendChild(tablePriceValue);

    const removeRow = document.createElement('td');
    const removeButton = document.createTextNode(removeButtonText);
    removeRow.appendChild(removeButton);

    newTr.appendChild(tableProduct);
    newTr.appendChild(tablePrice);
    newTr.appendChild(removeRow);
    productList.appendChild(newTr);

    //update total price
    updateTotalPrice(price);
}

function updateTotalPrice(price, isSum) {
    let newPrice = 0;
    
    newPrice = (parseFloat(totalPrice.innerHTML) + parseFloat(price)).toFixed(2);
    totalPrice.innerHTML = newPrice;
}

function removeProductFromList(event) {
    event.preventDefault();

    updateTotalPrice(event.target.previousSibling.innerHTML);
    productList.removeChild(event.target.parentElement);
}