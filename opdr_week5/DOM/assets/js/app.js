// 1. Bereken met JavaScript het gemiddelde op basis van de cijfers die in de tabel staan
//en plaats dit gemiddelde cijfer weer op een nieuwe tabelregel in de tabel (ook weer met JavaScript).
const transferGrades = document.getElementsByClassName('grade');

let gradeArray = [];
for(let i = 0; i < transferGrades.length; i++) {
    gradeArray.push(transferGrades[i].innerHTML);
}
console.log(gradeArray);

const averageGrade = function(gradeArray) {
    let gradesSum = 0;
    for(let i = 0; i < gradeArray.length; i++) {
        gradesSum += parseFloat(gradeArray[i]);
    }
    return gradesSum / gradeArray.length;
}
console.log(averageGrade(gradeArray));


/* 2.	Gegeven is de volgende HTML. 
   <ul id="course">
       <li class="course1">Computerscience basics</li>
       <li class="course2">Programming basics</li>
       <li class="course3">Studieloopbaan orientatie</li>
       <li class="course4">Gamedevelopment with TypeScript</li>
       <li class="course5">Professional Skills 1</li>
       <li class="course6">HZ personality 2a</li>
       <li class="course7">Framework development 1</li>
   </ul>

Zorg ervoor dat alle even elementen een andere achtergrondkleur krijgen. Gebruik hiervoor JS en maak zoveel mogelijke gebruik van functies.*/
const ListItems = document.getElementsByTagName('li');

const changeBackgroundEvenNumbers = function() {
    for(let i = 0; i < ListItems.length; i++) {
        if(i % 2 == 0) {
            ListItems[i].classList.add('greenBackground');
        }
    }
}
changeBackgroundEvenNumbers();


/* 3.	Maak een functie createImageElement(‘imageSrcName’) die een plaatje in JS maakt(img tag genereert met bijbehorende attributen)
en deze vervolgens in het DOM zet.*/
const addImage = function(imageSource) {
    const location = document.getElementById('body');
    const newImage = document.createElement('img');
    newImage.src = imageSource;
    newImage.alt = 'Heinrich';

    location.appendChild(newImage);
}

addImage('../DOM/assets/css/images/heinrich2.jpg');